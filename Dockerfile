FROM amazonlinux:latest

#install
RUN yum install httpd php rsync wget ssh -y

# configure
COPY code/* /var/www/html/


CMD ["sh", "-c", "tail -f /dev/null"]

CMD ["/usr/sbin/httpd","-D","FOREGROUND"]


EXPOSE 80